package com.subdue.autoconfigamajigger;

import static com.subdue.autoconfigamajigger.ConfigamajigUtil.*;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.subdue.autoconfigamajigger.ExecUtil.ExecResult;

public class ContainerUtil {
	private static final Logger log = LoggerFactory.getLogger(ContainerUtil.class);

	private final Map<String,String> roleContainerMap = new LinkedHashMap<String,String>();
	private final String hostname;
	private final String hostUuid;

	public ContainerUtil(String hostname, String hostUuid) {
		this.hostname = hostname;
		this.hostUuid = hostUuid;
	}

	public void createContainerForRole(String roleName) throws Exception {
		String existingDockerId = roleContainerMap.get(roleName);

		if(!StringUtils.isEmpty(existingDockerId)) {
			throw new Exception("Tried to create container for role: " + roleName + " when one already exists: " + existingDockerId);
		}

		String imageId = getDockerImageId(roleName, hostname);
		String[] command = new String[] {"/bin/bash", "-c", "echo \"127.0.0.1\t`hostname`\" | tee -a /etc/hosts && /bin/bash"};

		if(StringUtils.isEmpty(imageId)) {
			imageId = createBaseImageForRole(roleName);

			if(StringUtils.isEmpty(imageId)) {
				throw new Exception("Couldn't create base image for role: " + roleName);
			}
		}

		String userData = configGet(formatRoleKey(RoleClassifier.CONTAINER, roleName, RoleKey.USER_DATA));

		if(!StringUtils.isEmpty(userData)) {
			String userDataProps = configGet(formatHostRoleKey(hostname, RoleClassifier.CONTAINER, roleName, HostRoleKey.USER_DATA_PROPS));

			if(StringUtils.isEmpty(userDataProps)) {
				userDataProps = configGet(formatRoleKey(RoleClassifier.CONTAINER, roleName, RoleKey.USER_DATA_PROPS));
			}

			if(!StringUtils.isEmpty(userDataProps)) {
				userData = VelocityUtil.customizeUserData(userData, userDataProps);
			}

			imageId = createCustomizedImageForRole(roleName, imageId, userData);
			command = new String[] {"/bin/bash", "-c", "echo \"127.0.0.1\t`hostname`\" | tee -a /etc/hosts && chmod u+x /.user-data.sh && /.user-data.sh && /bin/bash"};
		}

		if(StringUtils.isEmpty(imageId)) {
			throw new Exception("Couldn't create image for role: " + roleName);
		}

		command = ArrayUtils.addAll(new String[] {"docker", "run", "-d", "-t", "-i", imageId}, command);

		ExecResult dockerResult = new ExecUtil()
			.setMergeErrorsToOutput(true)
			.handleExecute(command);
		int dockerExitCode = dockerResult.getProcess().exitValue();
		String dockerOutput = dockerResult.getOutputAsString();

		if(dockerExitCode != 0) {
			throw new Exception("Exit code from docker run: " + dockerExitCode + ", output: " + dockerOutput);
		}

		if(log.isDebugEnabled()) {
			log.debug("Result of docker run for image [" + imageId + "] - exitCode: " + dockerExitCode + ", output: " + dockerOutput);
		}

		String dockerId = new StringTokenizer(dockerOutput).nextToken();

		log.info("Created container for role: " + roleName + ", id: " + dockerOutput);
		roleContainerMap.put(roleName, dockerId);
	}

	public void removeContainerForRole(String roleName) throws Exception {
		String existingDockerId = roleContainerMap.get(roleName);

		if(StringUtils.isEmpty(existingDockerId)) {
			throw new Exception("Tried to remove container for role: " + roleName + " but none exists");
		}

		ExecResult dockerStopResult = new ExecUtil()
			.setMergeErrorsToOutput(true)
			.handleExecute("docker", "stop", existingDockerId);
		int dockerStopExitCode = dockerStopResult.getProcess().exitValue();
		String dockerStopOutput = dockerStopResult.getOutputAsString();

		if(dockerStopExitCode != 0) {
			throw new Exception("Exit code from docker stop: " + dockerStopExitCode + ", output: " + dockerStopOutput);
		}

		if(log.isDebugEnabled()) {
			log.debug("Result of docker stop for container [" + existingDockerId + "] - exitCode: " + dockerStopExitCode + ", output: " + dockerStopOutput);
		}

		ExecResult dockerRmResult = new ExecUtil()
			.setMergeErrorsToOutput(true)
			.handleExecute("docker", "rm", "-v", existingDockerId);
		int dockerRmExitCode = dockerRmResult.getProcess().exitValue();
		String dockerRmOutput = dockerRmResult.getOutputAsString();

		if(dockerRmExitCode != 0) {
			throw new Exception("Exit code from docker rm: " + dockerRmExitCode + ", output: " + dockerRmOutput);
		}

		if(log.isDebugEnabled()) {
			log.debug("Result of docker rm for container [" + existingDockerId + "] - exitCode: " + dockerRmExitCode + ", output: " + dockerRmOutput);
		}

		log.info("Removed container for role: " + roleName + ", id: " + existingDockerId);
		roleContainerMap.remove(roleName);
	}

	private String createCustomizedImageForRole(String roleName, String baseImageId, String userData) throws Exception {
		File userDataDir = createTempDir(roleName, "userData");
		File userDataFile = new File(userDataDir, "user-data.sh");

		FileUtils.writeStringToFile(userDataFile, userData);

		File dockerFile = new File(userDataDir, "Dockerfile");
		String lineSeparator = System.getProperty("line.separator");

		FileUtils.writeStringToFile(dockerFile, "FROM " + baseImageId + lineSeparator);
		FileUtils.writeStringToFile(dockerFile, "ADD user-data.sh /.user-data.sh" + lineSeparator, true);

		try {
			createDockerImage(dockerFile, roleName, hostUuid);

			return getDockerImageId(roleName, hostUuid);
		} finally {
			if(!log.isDebugEnabled()) {
				userDataFile.delete();
				dockerFile.delete();
				userDataDir.delete();
			}
		}
	}

	private String createBaseImageForRole(String roleName) throws Exception {
		String bootstrap = configGet(formatRoleKey(RoleClassifier.CONTAINER, roleName, RoleKey.BOOTSTRAP));

		if(StringUtils.isEmpty(bootstrap)) {
			throw new Exception("No bootstrap found for role: " + roleName);
		}

		File bootstrapDir = createTempDir(roleName, "bootstrap");
		File dockerFile = new File(bootstrapDir, "Dockerfile");

		FileUtils.writeStringToFile(dockerFile, bootstrap);

		try {
			createDockerImage(dockerFile, roleName, hostname);

			return getDockerImageId(roleName, hostname);
		} finally {
			if(!log.isDebugEnabled()) {
				dockerFile.delete();
				bootstrapDir.delete();
			}
		}
	}

	// hi i'm a vulnerability
	private File createTempDir(String prefix, String suffix) throws Exception {
		File tempDir = File.createTempFile(prefix, suffix);

		if(!tempDir.delete()) {
			throw new Exception("Couldn't delete temp file: " + tempDir.getAbsolutePath());
		}

		if(!tempDir.mkdir()) {
			throw new Exception("Couldn't create temp directory: " + tempDir.getAbsolutePath());
		}

		return tempDir;
	}

	private String getDockerImageId(String repository, String tag) throws Exception {
		ExecResult dockerImagesResult = new ExecUtil()
			.setMergeErrorsToOutput(true)
			.handleExecute("docker", "images", "-notrunc");
		int dockerImagesExitCode = dockerImagesResult.getProcess().exitValue();
		String dockerImagesOutput = dockerImagesResult.getOutputAsString();

		if(dockerImagesExitCode != 0) {
			throw new Exception("Exit code from docker images: " + dockerImagesExitCode + ", output: " + dockerImagesOutput);
		}

		if(log.isDebugEnabled()) {
			log.debug("Result of docker images for tag [" + repository + ":" + tag + "] - exitCode: " + dockerImagesExitCode + ", output: " + dockerImagesOutput);
		}

		Scanner imageScanner = new Scanner(dockerImagesOutput);

		try {
			while(imageScanner.hasNext()) {
				String scannedRepository = imageScanner.next();
				String scannedTag = imageScanner.next();
				String scannedId = imageScanner.next();

				if(!StringUtils.isEmpty(scannedRepository) && !StringUtils.isEmpty(scannedTag) && !StringUtils.isEmpty(scannedId)) {
					if(repository.equals(scannedRepository)) {
						if(tag.equals(scannedTag)) {
							return scannedId;
						}
					}
				}

				imageScanner.nextLine();
			}
		} finally {
			imageScanner.close();
		}

		return null;
	}

	private void createDockerImage(File dockerFile, String repository, String tag) throws Exception {
		ExecResult dockerBuildResult = new ExecUtil()
			.setMergeErrorsToOutput(true)
			.handleExecute(dockerFile.getParentFile(), "docker", "build", "-t", repository + ":" + tag, ".");
		int dockerBuildExitCode = dockerBuildResult.getProcess().exitValue();
		String dockerBuildOutput = dockerBuildResult.getOutputAsString();

		if(dockerBuildExitCode != 0) {
			throw new Exception("Exit code from docker build: " + dockerBuildExitCode + ", output: " + dockerBuildOutput);
		}

		if(log.isDebugEnabled()) {
			log.debug("Result of docker build for tag [" + repository + ":" + tag + "] - exitCode: " + dockerBuildExitCode + ", output: " + dockerBuildOutput);
		}
	}
}
