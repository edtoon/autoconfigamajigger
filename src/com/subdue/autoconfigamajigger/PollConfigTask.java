package com.subdue.autoconfigamajigger;

import static com.subdue.autoconfigamajigger.ConfigamajigUtil.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PollConfigTask extends TimerTask {
	private static final Logger log = LoggerFactory.getLogger(PollConfigTask.class);

	public static final String LOG_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss z";

	private final List<String> roles;
	private final String localHostname;
	private final Timer timer;
	private final boolean checkHostRoles;

	private LocalUtil localUtil = null;
	private ContainerUtil containerUtil = null;
	private String hostUuid = null;
	private Date startDate = null;
	private Date shutdownRequestedDate = null;
	private boolean enabledDefaultRoles = false;
	private boolean executing = false;

	public PollConfigTask(String localHostname, Timer timer) {
		this(localHostname, timer, null);
	}

	public PollConfigTask(String localHostname, Timer timer, List<String> roles) {
		this.localHostname = localHostname;
		this.timer = timer;
		this.roles = roles;

		if(roles == null || roles.isEmpty()) {
			checkHostRoles = true;
		} else {
			checkHostRoles = false;
		}
	}

	public void pollConfiguration() throws Exception {
		checkUuid();
		checkStartDate();
		checkHostnameChanged();
		checkShutdownRequested();

		if(shutdownRequestedDate != null) {
			try {
				configSet(formatHostKey(localHostname, hostUuid, HostKey.SHUTDOWN), "" + System.currentTimeMillis());
			} finally {
				timer.cancel();
				timer.purge();
			}

			return;
		}

		if(checkHostRoles) {
			checkHostRoles();
		} else if(!enabledDefaultRoles) {
			enableDefaultRoles();
			enabledDefaultRoles = true;
		}
	}

	private void enableDefaultRoles() {
		for(String defaultRole : roles) {
			int colonIdx = defaultRole.indexOf(':');
			String role = (colonIdx < 0 ? defaultRole : defaultRole.substring(colonIdx + 1));
			String classifierStr = (colonIdx < 0 ? "" : defaultRole.substring(0, colonIdx));
			RoleClassifier classifier = RoleClassifier.getByValue(classifierStr);
			boolean enableResult = false;

			try {
				enableResult = enableRole(classifier, role);
			} catch(Exception e) {
				log.error("Error enabling role: " + defaultRole, e);
			}

			if(!enableResult) {
				log.error("Didn't know how to enable role: " + defaultRole);
			}
		}
	}

	public synchronized void requestShutdown(String reason) throws Exception {
		if(shutdownRequestedDate == null) {
			log.error("Shutdown requested because: " + reason);
			shutdownRequestedDate = new Date();
		} else {
			log.error("Additional shutdown reason: " + reason);
		}
	}

	private void checkHostRoles() throws Exception {
		List<String> hostRoles = getHostRoles();
		int addedRoleCount = addNewHostRoles(hostRoles);
		int removedRoleCount = removeOldHostRoles(hostRoles);

		if(addedRoleCount > 0 || removedRoleCount > 0) {
			log.info("Added " + addedRoleCount + " roles, removed " + removedRoleCount + " roles, " + roles.size() + " current roles");
		} else if (log.isDebugEnabled()) {
			log.debug("No updates, " + roles.size() + " current roles");
		}
	}

	private int addNewHostRoles(List<String> hostRoles) {
		int addedRoleCount = 0;

		for(String hostRole : hostRoles) {
			int colonIdx = hostRole.indexOf(':');
			String role = (colonIdx < 0 ? hostRole : hostRole.substring(colonIdx + 1));

			if(StringUtils.isEmpty(role)) {
				log.warn("Ignoring new role because of empty role value: " + hostRole);
				continue;
			}

			String classifierStr = (colonIdx < 0 ? "" : hostRole.substring(0, colonIdx));
			RoleClassifier classifier = RoleClassifier.getByValue(classifierStr);

			if(classifier == null) {
				log.warn("Ignoring new role because of unrecognized classifier: " + hostRole);
			}

			if(!roles.contains(hostRole)) {
				log.info("Adding new role: " + hostRole);

				boolean enableResult = false;

				try {
					enableResult = enableRole(classifier, role);
				} catch(Exception e) {
					log.error("Error enabling role: " + hostRole, e);
				}

				if(enableResult) {
					roles.add(hostRole);
					addedRoleCount++;
				} else {
					log.error("Didn't know how to enable role: " + hostRole);
				}
			}
		}

		return addedRoleCount;
	}

	private int removeOldHostRoles(List<String> hostRoles) {
		int removedRoleCount = 0;

		for(final Iterator<String> i = roles.iterator(); i.hasNext(); ) {
			String existingRole = i.next();
			int colonIdx = existingRole.indexOf(':');
			String role = (colonIdx < 0 ? existingRole : existingRole.substring(colonIdx + 1));
			String classifierStr = (colonIdx < 0 ? "" : existingRole.substring(0, colonIdx));
			RoleClassifier classifier = RoleClassifier.getByValue(classifierStr);

			if(!hostRoles.contains(existingRole)) {
				log.info("Removing old role: " + existingRole);

				boolean disableResult = false;

				try {
					disableResult = disableRole(classifier, role);
				} catch(Exception e) {
					log.error("Error disabling role: " + existingRole);
				}

				if(disableResult) {
					i.remove();
					removedRoleCount++;
				} else {
					log.error("Didn't know how to disable role: " + existingRole);
				}
			}
		}

		return removedRoleCount;
	}

	private boolean enableRole(RoleClassifier classifier, String role) throws Exception {
		if(RoleClassifier.CONTAINER == classifier) {
			getContainerUtil().createContainerForRole(role);

			return true;
		} else if(RoleClassifier.LOCAL == classifier) {
			getLocalUtil().initializeRole(role);

			return true;
		}

		return false;
	}

	private boolean disableRole(RoleClassifier classifier, String role) throws Exception {
		if(RoleClassifier.CONTAINER == classifier) {
			getContainerUtil().removeContainerForRole(role);

			return true;
		}

		return false;
	}

	private List<String> getHostRoles() throws Exception {
		List<String> hostRoles = new LinkedList<String>();
		String rolesString = configGet(formatHostKey(localHostname, HostKey.ROLES));

		if(!StringUtils.isEmpty(rolesString)) {
			StringTokenizer roleTokens = new StringTokenizer(rolesString, CONFIG_ROLE_SEPARATOR);

			while(roleTokens.hasMoreTokens()) {
				String role = roleTokens.nextToken().trim();

				if(!StringUtils.isEmpty(role)) {
					hostRoles.add(role);
				}
			}
		}

		return hostRoles;
	}

	private void checkShutdownRequested() throws Exception {
		if(shutdownRequestedDate != null) {
			return;
		}

		String externalShutdownRequestTime = configGet(formatHostKey(localHostname, HostKey.SHUTDOWN));

		if(StringUtils.isEmpty(externalShutdownRequestTime) || !StringUtils.isNumeric(externalShutdownRequestTime)) {
			return;
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(LOG_DATE_FORMAT);
		Date externalShutdownRequestDate = new Date(Long.parseLong(externalShutdownRequestTime));
		log.info("Shutdown requested externally at: " + simpleDateFormat.format(externalShutdownRequestDate));

		shutdownRequestedDate = externalShutdownRequestDate;
	}

	private void checkHostnameChanged() throws UnknownHostException, Exception {
		String currentLocalHostname = InetAddress.getLocalHost().getHostName();

		if(!localHostname.equals(currentLocalHostname)) {
			requestShutdown("Local hostname changed to " + currentLocalHostname);
		}
	}

	private void checkStartDate() throws Exception {
		String existingStartTime = configGet(formatHostKey(localHostname, HostKey.START));
		boolean configHadStartTime = (!StringUtils.isEmpty(existingStartTime) && StringUtils.isNumeric(existingStartTime));
		Date existingStartDate = (configHadStartTime ? new Date(Long.parseLong(existingStartTime)) : null);

		if(startDate != null) {
			if(existingStartDate != null && !startDate.equals(existingStartDate)) {
				requestShutdown("Another instance has started since start date");
			}

			return;
		}

		startDate = new Date();

		String startTimeString = "" + startDate.getTime();

		configSet(formatHostKey(localHostname, HostKey.START), startTimeString);

		if(log.isDebugEnabled()) {
			log.debug("Set start date to: " + startTimeString);
		}

		if(existingStartDate != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(LOG_DATE_FORMAT);

			log.warn("Previously started at: " + simpleDateFormat.format(existingStartDate));
		}
	}

	private void checkUuid() throws Exception {
		String existingUuid = configGet(formatHostKey(localHostname, HostKey.UUID));
		boolean configHadUuid = (!StringUtils.isEmpty(existingUuid));

		if(hostUuid != null) {
			if(configHadUuid && !hostUuid.equals(existingUuid)) {
				requestShutdown("Another instance has been created since this instance");
			}

			return;
		}

		hostUuid = UUID.randomUUID().toString();

		configSet(formatHostKey(localHostname, HostKey.UUID), hostUuid);
		configSet(formatHostKey(localHostname, HostKey.SHUTDOWN), "");

		if(log.isDebugEnabled()) {
			log.debug("Set hostUuid to: " + hostUuid);
		}

		if(configHadUuid) {
			log.warn("Previously had the uuid: " + existingUuid);
		}
	}

	private synchronized LocalUtil getLocalUtil() {
		if(localUtil == null) {
			localUtil = new LocalUtil(localHostname, hostUuid);
		}

		return localUtil;
	}

	private synchronized ContainerUtil getContainerUtil() {
		if(containerUtil == null) {
			containerUtil = new ContainerUtil(localHostname, hostUuid);
		}

		return containerUtil;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getShutdownRequestedDate() {
		return shutdownRequestedDate;
	}

	public void setShutdownRequestedDate(Date shutdownRequestedDate) {
		this.shutdownRequestedDate = shutdownRequestedDate;
	}

	public boolean getExecuting() {
		return executing;
	}

	public void run() {
		try {
			executing = true;

			try {
				pollConfiguration();
			} finally {
				executing = false;
			}
		} catch(Exception e) {
			log.error("Error polling for configuration", e);
		}
	}
}
