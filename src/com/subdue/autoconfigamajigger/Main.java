package com.subdue.autoconfigamajigger;

import static com.subdue.autoconfigamajigger.ConfigamajigUtil.*;

import java.io.IOException;
import java.net.InetAddress;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Timer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main extends AbstractHandler {
	private static final Logger log = LoggerFactory.getLogger(Main.class);

	private static final long POLL_CONFIG_INTERVAL = 1 * 60 * 1000;

	private final Server server;
	private final Timer timer;
	private final PollConfigTask pollConfigTask;

	public Main(String localHostname, int listenPort, List<String> roleList) throws Exception {
		Server server = new Server();
		Connector connector = new SelectChannelConnector();

		connector.setHost("localhost");
		connector.setPort(listenPort);
		server.addConnector(connector);
		server.setHandler(this);

		this.server = server;
		this.timer = new Timer();
		this.pollConfigTask = new PollConfigTask(localHostname, timer, roleList);
	}

	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		baseRequest.setHandled(true);

		if(log.isDebugEnabled()) {
			log.debug(request.getMethod() + " from: " + request.getRemoteAddr() + " for target: " + target);
		}

		try {
			if("GET".equals(request.getMethod())) {
			}
		} catch(Exception e) {
			log.error("Error processing " + request.getMethod() + " for target: " + target, e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().println("<b>Request failed. Sorry.</b>");
		}

		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().println("<b>Not found: </b> " + target);
	}

	public void startService() throws Exception {
		server.start();
		timer.schedule(pollConfigTask, 0, POLL_CONFIG_INTERVAL);
	}

	public Server getServer() {
		return server;
	}

	public Timer getTimer() {
		return timer;
	}

	public PollConfigTask getPollConfigTask() {
		return pollConfigTask;
	}

	public static void main(String[] args) throws Exception {
		String url = System.getProperty("MAJIG_URL");
		String password = System.getProperty("MAJIG_PASSWORD");
		String salt64 = System.getProperty("MAJIG_SALT64");
		String port = System.getProperty("AUTO_PORT");
		String roles = System.getProperty("AUTO_ROLES");
		String host = InetAddress.getLocalHost().getHostName();

		if(StringUtils.isEmpty(url)) {
			throw new Exception("Missing url");
		}

		if(StringUtils.isEmpty(password)) {
			throw new Exception("Missing password");
		}

		if(StringUtils.isEmpty(salt64)) {
			throw new Exception("Missing salt64");
		}

		if(StringUtils.isEmpty(port) || !StringUtils.isNumeric(port)) {
			throw new Exception("Missing or invalid port");
		}

		if(StringUtils.isEmpty(host)) {
			throw new Exception("Could not determine host name");
		}

		log.info("Starting on host: " + host + (StringUtils.isEmpty(roles) ? "" : ", roles: " + roles));

		if(log.isDebugEnabled()) {
			log.debug("MAJIG_URL: " + url);
			log.debug("MAJIG_PASSWORD: " + password);
			log.debug("MAJIG_SALT64: " + salt64);
		}

		List<String> roleList = new LinkedList<String>();

		if(!StringUtils.isEmpty(roles)) {
			StringTokenizer roleTokens = new StringTokenizer(roles, CONFIG_ROLE_SEPARATOR);

			while(roleTokens.hasMoreTokens()) {
				String role = roleTokens.nextToken().trim();

				if(!StringUtils.isEmpty(role)) {
					roleList.add(role);
				}
			}
		}

		Main main = new Main(host, Integer.parseInt(port), roleList);
		Server server = main.getServer();
		PollConfigTask pollConfigTask = main.getPollConfigTask();

		main.startService();

		while(pollConfigTask.getExecuting() || (pollConfigTask.getShutdownRequestedDate() == null)) {
			try {
				Thread.sleep(1000);
			} catch(InterruptedException ie) { }
		}

		server.stop();
		server.join();
	}
}
