package com.subdue.autoconfigamajigger;

import static com.subdue.autoconfigamajigger.ConfigamajigUtil.*;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.subdue.autoconfigamajigger.ExecUtil.ExecResult;

public class LocalUtil {
	private static final Logger log = LoggerFactory.getLogger(LocalUtil.class);

	private final String hostname;
	private final String hostUuid;

	public LocalUtil(String hostname, String hostUuid) {
		this.hostname = hostname;
		this.hostUuid = hostUuid;
	}

	public void initializeRole(String roleName) throws Exception {
		String userData = configGet(formatRoleKey(RoleClassifier.LOCAL, roleName, RoleKey.USER_DATA));

		if(StringUtils.isEmpty(userData)) {
			throw new Exception("Don't know how to initialize role: " + roleName);
		}

		String userDataProps = configGet(formatHostRoleKey(hostname, RoleClassifier.LOCAL, roleName, HostRoleKey.USER_DATA_PROPS));

		if(StringUtils.isEmpty(userDataProps)) {
			userDataProps = configGet(formatRoleKey(RoleClassifier.LOCAL, roleName, RoleKey.USER_DATA_PROPS));
		}

		if(!StringUtils.isEmpty(userDataProps)) {
			log.debug("Using user data props:\n\n" + userDataProps + "\n");

			userData = VelocityUtil.customizeUserData(userData, userDataProps);
		}

		File userDataFile = File.createTempFile(roleName.replace(CONFIG_ROLE_SEPARATOR, "_"), "user-data");

		FileUtils.writeStringToFile(userDataFile, userData);

		userDataFile.setExecutable(true);

		ExecResult userDataResult = new ExecUtil()
			.setMergeErrorsToOutput(true)
			.handleExecute("sudo", userDataFile.getAbsolutePath());
		int userDataExitCode = userDataResult.getProcess().exitValue();
		String userDataOutput = userDataResult.getOutputAsString();

		if(userDataExitCode != 0) {
			throw new Exception("Exit code from user-data run: " + userDataExitCode + ", output: " + userDataOutput);
		}

		if(log.isDebugEnabled()) {
			log.debug("Result of user-data run - exitCode: " + userDataExitCode + ", output: " + userDataOutput);
		} else {
			userDataFile.delete();
		}

		log.info("Initialized local role: " + roleName + " for host: " + hostname + ", uuid: " + hostUuid);
	}
}
