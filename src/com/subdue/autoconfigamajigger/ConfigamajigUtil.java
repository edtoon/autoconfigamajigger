package com.subdue.autoconfigamajigger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.subdue.autoconfigamajigger.CryptoUtil.SharedData;

public class ConfigamajigUtil {
	private static final Logger log = LoggerFactory.getLogger(ConfigamajigUtil.class);

	public enum HostRoleKey {
		USER_DATA_PROPS("user-data-props");

		private final String value;

		private HostRoleKey(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum RoleKey {
		BOOTSTRAP("bootstrap"), USER_DATA("user-data"), USER_DATA_PROPS("user-data-props");

		private final String value;

		private RoleKey(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum RoleClassifier {
		CONTAINER("container"), LOCAL("local");

		private final String value;

		private RoleClassifier(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public static RoleClassifier getByValue(String value) {
			for(RoleClassifier classifier : values()) {
				if(classifier.getValue().equals(value)) {
					return classifier;
				}
			}

			return null;
		}
	}

	public enum HostKey {
		START("start"), UUID("uuid"), ROLES("roles"), SHUTDOWN("shutdown");

		private final String value;

		private HostKey(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public static final String CONFIG_ROLE_SEPARATOR = ";";

	public static final String formatHostRoleKey(String hostname, RoleClassifier classifier, String roleName, HostRoleKey hostRoleKey) {
		return "/hosts/" + hostname + "/roles/" + classifier.getValue() + ":" + roleName + "/" + hostRoleKey.getValue();
	}

	public static final String formatRoleKey(RoleClassifier classifier, String roleName, RoleKey roleKey) {
		return "/role/" + classifier.getValue() + ":" + roleName + "/" + roleKey.getValue();
	}

	public static final String formatHostKey(String hostname, HostKey hostKey) {
		return "/hosts/" + hostname + "/" + hostKey.getValue();
	}

	public static final String formatHostKey(String hostname, String hostUuid, HostKey hostKey) {
		return "/hosts/" + hostname + "/" + hostUuid + "/" + hostKey.getValue();
	}

	public static String configGet(String key) throws Exception {
		String requestUrl = System.getProperty("MAJIG_URL") + key;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(requestUrl);
		CloseableHttpResponse httpGetResponse = httpClient.execute(httpGet);

		try {
			int statusCode = httpGetResponse.getStatusLine().getStatusCode();

			if(HttpServletResponse.SC_NOT_FOUND == statusCode) {
				return null;
			}

			if(HttpServletResponse.SC_OK != statusCode) {
				throw new Exception("Get status: " + statusCode);
			}

			StringBuffer responseBuffer = new StringBuffer();
			HttpEntity responseEntity = httpGetResponse.getEntity();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(responseEntity.getContent()));

			try {
				String responseLine;

				while((responseLine = bufferedReader.readLine()) != null) {
					responseBuffer.append(responseLine);
				}
			} finally {
				bufferedReader.close();
			}

			String response = responseBuffer.toString();
			String[] pairs = response.split("&");
			String iv64Encoded = null;
			String cipher64Encoded = null;

			for(String pair : pairs) {
				int idx = pair.indexOf("=");
				String param = URLDecoder.decode(pair.substring(0, idx), "UTF-8");
				String paramValue = URLDecoder.decode(pair.substring(idx + 1), "UTF-8");

				if("iv64".equals(param)) {
					iv64Encoded = paramValue;
				} else if("cipher64".equals(param)) {
					cipher64Encoded = paramValue;
				}
			}

			SharedData sharedData = new SharedData(Base64.decodeBase64(iv64Encoded), Base64.decodeBase64(cipher64Encoded));
			char[] password = System.getProperty("MAJIG_PASSWORD").toCharArray();
			byte[] salt = Base64.decodeBase64(System.getProperty("MAJIG_SALT64"));

			return CryptoUtil.decrypt(password, salt, sharedData);
		} finally {
			try {
				httpGetResponse.close();
			} catch(Exception e) {
				log.warn("Error closing response from GET to url: " + requestUrl, e);
			}
		}
	}

	public static void configSet(String key, String value) throws Exception {
		char[] password = System.getProperty("MAJIG_PASSWORD").toCharArray();
		byte[] salt = Base64.decodeBase64(System.getProperty("MAJIG_SALT64"));
		SharedData sharedData = CryptoUtil.encrypt(password, salt, value);
		String iv64 = Base64.encodeBase64String(sharedData.getIv());
		String cipher64 = Base64.encodeBase64String(sharedData.getCiphertext());
		String requestUrl = System.getProperty("MAJIG_URL") + key;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(requestUrl);
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("iv64", iv64));
		nameValuePairs.add(new BasicNameValuePair("cipher64", cipher64));
		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		CloseableHttpResponse httpPostResponse = httpClient.execute(httpPost);

		try {
			int statusCode = httpPostResponse.getStatusLine().getStatusCode();

			if(HttpServletResponse.SC_OK != statusCode) {
				throw new Exception("Post status: " + statusCode);
			}
		} finally {
			try {
				httpPostResponse.close();
			} catch(Exception e) {
				log.warn("Error closing response from POST to url: " + requestUrl, e);
			}
		}
	}
}
