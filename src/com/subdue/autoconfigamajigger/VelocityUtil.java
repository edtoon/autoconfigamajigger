package com.subdue.autoconfigamajigger;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class VelocityUtil {
	private VelocityUtil() { }

	public static String customizeUserData(String userData, String userDataProps) throws Exception {
		Properties properties = new Properties();

		properties.load(new StringReader(userDataProps));

		VelocityContext velocityContext = new VelocityContext(properties);
		StringWriter stringWriter = new StringWriter();

		if(!Velocity.evaluate(velocityContext, stringWriter, "customizeUserData", userData)) {
			throw new Exception("Failed to parse user data properties");
		}

		return stringWriter.toString();
	}
}
