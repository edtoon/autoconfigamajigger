package com.subdue.autoconfigamajigger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExecUtil {
	private static final Logger log = LoggerFactory.getLogger(Main.class);

	public static class ExecResult {
		private final Process process;
		private final ByteArrayOutputStream outputStream;
		private final ByteArrayOutputStream errorStream;

		private Thread outputThread = null;
		private Thread errorThread = null;

		public ExecResult(Process process, ByteArrayOutputStream outputStream, ByteArrayOutputStream errorStream, Thread outputThread, Thread errorThread) {
			this.process = process;
			this.outputStream = outputStream;
			this.errorStream = errorStream;
			this.outputThread = outputThread;
			this.errorThread = errorThread;
		}

		public Process getProcess() {
			return process;
		}

		public ByteArrayOutputStream getOutputStream() {
			return outputStream;
		}

		public Thread getOutputThread() {
			return outputThread;
		}

		public String getOutputAsString() throws IOException {
			return getOutputAsString("ISO-8859-1");
		}

		public String getOutputAsString(final String encoding) throws UnsupportedEncodingException {
			if(outputStream == null) {
				return null;
			}

			joinIoCopyThreads();

			return new String(outputStream.toByteArray(), encoding);
		}

		public ByteArrayOutputStream getErrorStream() {
			return errorStream;
		}

		public Thread getErrorThread() {
			return errorThread;
		}

		public String getErrorAsString() throws IOException {
			return getErrorAsString("ISO-8859-1");
		}

		public String getErrorAsString(final String encoding) throws UnsupportedEncodingException {
			if(errorStream == null) {
				return null;
			}

			joinIoCopyThreads();

			return new String(errorStream.toByteArray(), encoding);
		}

		public synchronized void joinIoCopyThreads() {
			if(outputThread != null) {
				try {
					outputThread.join();
					outputThread = null;
				} catch(InterruptedException e) { }
			}
			if(errorThread != null) {
				try {
					errorThread.join();
					errorThread = null;
				} catch(InterruptedException e) { }
			}
		}
	}

	private Boolean verifyInput = null;
	private Boolean verifyExitCode = null;
	private Boolean verifyOutputExists = null;
	private Boolean includeOutput = null;
	private Boolean includeErrors = null;
	private Boolean mergeErrorsToOutput = null;
	private Boolean startOnly = null;

	private OutputStream outputStreamHandler = null;
	private OutputStream errorStreamHandler = null;

	private List<String> commandLine = null;
	private boolean validatedOptions = false;
	private boolean validatedInput = false;
	private boolean executed = false;

	public ExecUtil() { }

	public ExecUtil reset() {
		outputStreamHandler = null;
		errorStreamHandler = null;

		commandLine = null;
		validatedOptions = false;
		validatedInput = false;
		executed = false;

		return this;
	}

	public ExecUtil setVerifyInput(boolean verifyInput) {
		this.verifyInput = verifyInput;
		return this;
	}

	public ExecUtil setVerifyExitCode(boolean verifyExitCode) {
		this.verifyExitCode = verifyExitCode;
		return this;
	}

	public ExecUtil setVerifyOutputExists(boolean verifyOutputExists) {
		this.verifyOutputExists = verifyOutputExists;
		return this;
	}

	public ExecUtil setIncludeOutput(boolean includeOutput) {
		this.includeOutput = includeOutput;
		return this;
	}

	public ExecUtil setIncludeErrors(boolean includeErrors) {
		this.includeErrors = includeErrors;
		return this;
	}

	public ExecUtil setMergeErrorsToOutput(boolean mergeErrorsToOutput) {
		this.mergeErrorsToOutput = mergeErrorsToOutput;
		return this;
	}

	public ExecUtil setStartOnly(boolean startOnly) {
		this.startOnly = startOnly;
		return this;
	}

	public ExecUtil setOutputStreamHandler(OutputStream outputStreamHandler) {
		this.outputStreamHandler = outputStreamHandler;
		return this;
	}

	public ExecUtil setErrorStreamHandler(OutputStream errorStreamHandler) {
		this.errorStreamHandler = errorStreamHandler;
		return this;
	}

	public ExecResult handleExecute(String... commandArray) throws IOException, ExecException {
		return handleExecute(null, commandArray);
	}

	public ExecResult handleExecute(File workingDirectory, String... commandArray) throws IOException, ExecException {
		return handleExecute(workingDirectory, Arrays.asList(commandArray));
	}

	public ExecResult handleExecute(List<String> commandList) throws IOException, ExecException {
		return handleExecute(null, commandList);
	}

	public ExecResult handleExecute(File workingDirectory, List<String> commandList) throws IOException, ExecException {
		validateOptions();

		ProcessBuilder processBuilder = new ProcessBuilder(commandList);

		if(workingDirectory != null) {
			processBuilder.directory(workingDirectory);
		}

		return handleExecute(processBuilder);
	}

	public ExecResult handleExecute(ProcessBuilder processBuilder) throws IOException, ExecException {
		validateOptions();

		commandLine = processBuilder.command();

		validateInput();

		if(mergeErrorsToOutput) {
			processBuilder.redirectErrorStream(true);
		} else if(processBuilder.redirectErrorStream()) {
			throw new ExecException("ProcessBuilder wants to redirect stderr to stdout but we were told not to merge output");
		}

		if(log.isDebugEnabled()) {
			log.debug("Running command: " + commandLine.toString());
		}

		return handleExecute(processBuilder.start());
	}

	public ExecResult handleExecute(Process process) throws ExecException {
		if(!validatedOptions && mergeErrorsToOutput) {
			throw new ExecException("Cannot merge output when passed a process directly");
		}

		if(verifyInput == null) {
			verifyInput = false;
		}

		validateOptions();

		if(executed) {
			throw new ExecException("ExecHelper already executed, must be reset to use again");
		}

		executed = true;

		OutputStream outputStreamHandler = this.outputStreamHandler;
		OutputStream errorStreamHandler = this.errorStreamHandler;
		Thread outputThread = null;
		Thread errorThread = null;

		if(outputStreamHandler == null) {
			outputStreamHandler = (includeOutput ? new ByteArrayOutputStream() : NullOutputStream.NULL_OUTPUT_STREAM);
		}

		if(errorStreamHandler == null) {
			errorStreamHandler = (mergeErrorsToOutput ? null : (includeErrors ? new ByteArrayOutputStream() : NullOutputStream.NULL_OUTPUT_STREAM));
		}

		if(outputStreamHandler != null) {
			outputThread = createStreamCopier(process.getInputStream(), outputStreamHandler);
		}

		if(errorStreamHandler != null) {
			errorThread = createStreamCopier(process.getErrorStream(), errorStreamHandler);
		}

		ExecResult result = new ExecResult(
			process,
			(outputStreamHandler instanceof ByteArrayOutputStream) ? (ByteArrayOutputStream)outputStreamHandler : null,
			(errorStreamHandler instanceof ByteArrayOutputStream) ? (ByteArrayOutputStream)errorStreamHandler : null,
			outputThread,
			errorThread
		);
		String command = "<unknown>";
		String printableArguments = "<unknown>";

		if(commandLine != null && !commandLine.isEmpty()) {
			String commandPath = commandLine.get(0);
			int lastPathSeparator = commandPath.lastIndexOf(File.separatorChar);

			if(lastPathSeparator < 0) {
				command = commandPath;
			} else {
				command = commandPath.substring((lastPathSeparator < 0 ? 0 : (lastPathSeparator + 1)));
			}

			if(commandLine.size() <= 1) {
				printableArguments = "";
			} else {
				List<String> arguments = commandLine.subList(1, commandLine.size());

				printableArguments = arguments.toString();
			}
		}

		if(startOnly) {
			if(log.isDebugEnabled()) {
				log.debug("Started " + command + ", arguments: " + printableArguments);
			}
		} else {
			Integer exitCode = null;

			try {
				exitCode = process.waitFor();
			} catch(InterruptedException e) { }

			if(exitCode == null) {
				log.warn("Error starting command: " + command + ", arguments: " + printableArguments);

				throw new ExecException("Error starting command: " + command);
			}

			if(verifyExitCode) {
				if(exitCode.intValue() != 0) {
					String output = null;
					String errors = null;

					try {
						output = result.getOutputAsString();
					} catch(IOException e) {
						log.error("Error getting output for failed command: " + command + ", arguments: " + printableArguments, e);
					}

					if(!mergeErrorsToOutput) {
						try {
							errors = result.getErrorAsString();
						} catch(IOException e) {
							log.error("Error getting errors for failed command: " + command + ", arguments: " + printableArguments, e);
						}
					}

					log.warn("Bad exit code: " + exitCode + " for command: " + command + ", arguments: " + printableArguments +
						", output: " + output + ", errors: " + errors
					);

					throw new ExecException("Bad exit code: " + exitCode + " for command: " + command);
				}
			}

			if(verifyOutputExists) {
				result.joinIoCopyThreads();

				if(result.getOutputStream() == null || !(result.getOutputStream() instanceof ByteArrayOutputStream) ||
						((ByteArrayOutputStream)result.getOutputStream()).toByteArray().length == 0) {
					log.warn("No output from command: " + command + ", exit code: " + exitCode + ", arguments: " + printableArguments);

					throw new ExecException("No output from command: " + command + ", exit code: " + exitCode);
				}
			}

			if(log.isDebugEnabled()) {
				log.debug("Command: " + command + " exited with code: " + exitCode + ", arguments: " + printableArguments);
			}
		}

		return result;
	}

	private void validateInput() throws ExecException {
		if(validatedInput) {
			return;
		}

		if(verifyInput && (commandLine == null || commandLine.isEmpty())) {
			log.warn("No command line given");
			throw new ExecException("No command line given");
		}

		validatedInput = true;
	}

	private void validateOptions() throws ExecException {
		if(validatedOptions) {
			return;
		}

		applyDefaultsToNullOptions();

		if(startOnly && (verifyExitCode || verifyOutputExists)) {
			throw new ExecException("Cannot verify exit code or output when only starting a process.");
		}

		if(mergeErrorsToOutput && !includeErrors && includeOutput) {
			throw new ExecException("Cannot merge output without errors included.");
		}

		if(verifyOutputExists && !includeOutput) {
			throw new ExecException("Cannot verify output exists without output included.");
		}

		validatedOptions = true;
	}

	private void applyDefaultsToNullOptions() {
		if(verifyInput == null) {
			verifyInput = true;
		}

		if(startOnly == null) {
			startOnly = false;
		}

		if(verifyExitCode == null) {
			verifyExitCode = (!startOnly);
		}

		if(verifyOutputExists == null) {
			verifyOutputExists = false;
		}

		if(mergeErrorsToOutput == null) {
			mergeErrorsToOutput = false;
		}

		if(includeOutput == null) {
			includeOutput = (verifyOutputExists || !!mergeErrorsToOutput);
		}

		if(includeErrors == null) {
			includeErrors = (!!mergeErrorsToOutput);
		}
	}

	public static Thread createStreamCopier(final InputStream inputStream, final OutputStream outputStream) {
		Thread thread = new Thread(new Runnable() {
			public void run() {
				try {
					IOUtils.copy(inputStream, outputStream);
				} catch (IOException e) {}
			}
		});

		thread.start();

		return thread;
	}

	public static class ExecException extends Exception {
		private static final long serialVersionUID = 1L;

		public ExecException() {
			super();
		}

		public ExecException(String message, Throwable cause) {
			super(message, cause);
		}

		public ExecException(String message) {
			super(message);
		}

		public ExecException(Throwable cause) {
			super(cause);
		}
	}
}
