autoconfigamajigger
===================

Automatically do stuff your configamajig told you to.


How to run
==========

ant -DMAJIG_URL=https://my.configmajig.host/path -DMAJIG_PASSWORD=shared_secret -DMAJIG_SALT64=base64_salt -DAUTO_PORT=5432 run

You shouldn't really do that though. You shouldn't really use this either.
